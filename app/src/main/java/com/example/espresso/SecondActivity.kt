package com.example.espresso

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val intent = intent
        val welcomeTextDetails = intent.getStringExtra("details")

        val welcomeText = findViewById<TextView>(R.id.welcome_text_second_activity)
        if (welcomeTextDetails != null)
            welcomeText.text = welcomeTextDetails
    }
}
