package com.example.espresso

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val showButton = findViewById<Button>(R.id.show_text_button)
        showButton.setOnClickListener {
            val textView = findViewById<TextView>(R.id.show_text)
            textView.text = "Button Clicked"
        }

        val details = findViewById<EditText>(R.id.next_activity_edit_text)
        val nextActivityButton = findViewById<Button>(R.id.next_activity_button)
        nextActivityButton.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("details", details.text.toString())
            startActivity(intent)
        }
    }
}
