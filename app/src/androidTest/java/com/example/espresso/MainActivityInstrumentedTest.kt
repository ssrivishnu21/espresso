package com.example.espresso

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityInstrumentedTest {

    @get:Rule
    var mActivityTestRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java
    )

    @Test
    fun checkWhetherWelcomeTextIsDisplayed() {
        //check text is displayed which has id
        onView(withId(R.id.welcome_text)).check(matches(isDisplayed()))
        //check text is displayed which does not had an id
        onView(withText("Welcome To Espresso World")).check(matches(isDisplayed()))
    }

    @Test
    fun checkButtonIsClickedAndTextIsDisplayed() {
        //check text is not displayed
        onView(withText("Button Clicked")).check(doesNotExist())
        //click button
        onView(withId(R.id.show_text_button)).perform(click())
        //or if button has no id we can use this
        onView(withText("Show Text")).check(matches(isDisplayed())).perform(click())
        //check text is displayed
        onView(withText("Button Clicked")).check(matches(isDisplayed()))
    }

    @Test
    fun checkNextActivityButtonGoesToNextActivityWithEnteredText() {
        //to type the text in edit text field
        onView(withId(R.id.next_activity_edit_text)).perform(
            typeText("Welcome SomeOne"),
            closeSoftKeyboard()
        )
        //click next activity button
        onView(withId(R.id.next_activity_button)).perform(click())
        //check next activity is displayed
        onView(withText("Welcome SomeOne")).check(matches(isDisplayed()))
    }

}
