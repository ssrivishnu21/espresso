package com.example.espresso

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SecondActivityInstrumentationTest {
    @get:Rule
    var secondActivityTestRule: ActivityTestRule<SecondActivity> = ActivityTestRule(
        SecondActivity::class.java, false, false
    )

    @Test
    fun shouldCheckTextIsDisplayedPassedThroughIntent() {
        val intent = Intent()
        intent.putExtra("details", "Hello EveryOne")
        secondActivityTestRule.launchActivity(intent)

        onView(withText("Hello EveryOne")).check(matches(isDisplayed()))
    }
}